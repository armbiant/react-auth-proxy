import express from "express"
import supertest from "supertest"
import * as parse5 from "parse5"
import * as cookieJs from "cookie"

import proxy from "../src/index.js"

// For testing the HTML script
global.window = {
	location: {
		current: "/home",
		replace: url => window.location.current = url,
	}
}
//Possible shortcut in the browser
global.location = global.window.location

function listChildren(htmlNode)
{
	return htmlNode.childNodes.map(child => child.nodeName)
}

function getChild(htmlNode, tag)
{
	return htmlNode?.childNodes?.find(child => child.nodeName === tag)
}

function select(htmlNode, ...tags)
{
	return tags.reduce((current, child) => getChild(current, child), htmlNode)
}

async function createApiServer()
{
	const app = express()
	const server = supertest(app)

	// Technical middleware just to get supertest url for the proxy target setting
	app.get("/", (req, res) => res.end())
	const {request: {url}} = await server.get("/")

	app.use("/local-server", (req, res) =>
	{
		console.log("here")
		try
		{
			const user = JSON.parse(req.headers.authorization)
			console.log("the user", user)
			res.json(user.name)
		}
		catch(err)
		{
			console.log(err)
			res.statusCode = 501
			res.send("exception")
		}
	})
	console.log("the url", url)
	return [server, url]
}

function createApiServer2()
{
	const app = express()

	app.use("/local-server", (req, res) =>
	{
		console.log("here")
		try
		{
			const user = JSON.parse(req.headers.authorization)
			console.log("the user", user)
			res.json(user.name)
		}
		catch(err)
		{
			console.log(err)
			res.statusCode = 501
			res.send("exception")
		}
	})

	return app.listen(8080)
}

let apiServer

beforeAll(() =>
{
	apiServer = createApiServer2()
})

afterAll(() =>
{
	apiServer.close()
})

test("Default config complete flow", async () =>
{
	const app = express()
	const server = supertest(app)

	const registerer = proxy(/*{proxy: { target: testServerUrl }}*/)
	expect(registerer.config).not.toBeUndefined()
	registerer(app)

	console.log("the registerer.config", registerer.config)

	const redirect = registerer.config.auth.redirectParamName

	// No session yet
	const resServer = await server.get(registerer.config.auth.serverPath)
	expect(resServer.status).toEqual(302)
	expect(resServer.headers["location"]).toEqual(registerer.config.auth.loginPath + "?" + redirect + "=%2Flocal-server")

	// Setup made by the webapp before moving to the login page
	expect(window.location.current).toEqual("/home")
	window.location.search = redirect + "=" + window.location.current

	const resLogin = await server.get(registerer.config.auth.loginPath)
	//.query({ redirect: window.location.current })
	expect(resLogin.headers["content-type"]).toMatch("text/html")

	const document = parse5.parse(resLogin.text)
	const scriptLogin = new Function(
		select(document, "html", "body", "script", "#text")?.value + `; return login(${JSON.stringify(registerer.config.auth.presetUser[0])})`)

	scriptLogin()
	expect(window.location.current).toEqual(registerer.config.auth.successPath + "?" + redirect + "=%2Fhome&user=%7B%22name%22%3A%22test%22%7D")

	const resSuccess = await server.get(window.location.current)
	expect(resSuccess.status).toEqual(302)
	expect(resSuccess.headers.location).toEqual("/home")

	const setCookieResp = resSuccess.headers["set-cookie"][0]
	const cookie = cookieJs.parse(setCookieResp)
	//console.log(cookie)
	expect(cookie.session).toMatch(/.{6}/)
	expect(cookie["Max-Age"]).toEqual("604800")
	expect(cookie["Path"]).toEqual("/")

	// Next time the api server is requested, the API is directly called with the encoded user attached.
	const resServer2 = await server.get(registerer.config.auth.serverPath).set({cookie: setCookieResp})
	expect(resServer2.status).toEqual(200)
	expect(resServer2.body).toEqual(registerer.config.auth.presetUser[0].name)

	// Logging out removes the session cookie
	const resLogout = await server.get(registerer.config.auth.logoutPath).set({cookie: setCookieResp})
	expect(resLogout.status).toEqual(200)

	const detCookieResp = resLogout.headers["set-cookie"][0]
	const delCookie = cookieJs.parse(detCookieResp)
	expect(delCookie.session).toEqual("")
	expect(delCookie["Expires"]).toEqual("Thu, 01 Jan 1970 00:00:00 GMT")
	expect(delCookie["Path"]).toEqual("/")

	// Checking the logout redirection
	const logoutRedirUrl = "the-redir"
	const logoutParams = new URLSearchParams()
	logoutParams.set("post_logout_redirect_uri", logoutRedirUrl)
	const resLogoutRedir = await server.get(registerer.config.auth.logoutPath + "?" + logoutParams)
	expect(resLogoutRedir.status).toEqual(302)
	expect(resLogoutRedir.headers.location).toEqual(logoutRedirUrl)
})
